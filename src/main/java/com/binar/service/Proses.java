package com.binar.service;

import java.io.*;
import java.util.*;

public class Proses<var> {
    public <String>List<Integer> read(String path) {
        try {
            File file = new File(java.lang.String.valueOf(path));
            FileReader fr = new FileReader(file);
            BufferedReader reader = new BufferedReader(fr);

            java.lang.String line = " ";
            java.lang.String[] tempArr;

            List<Integer> listInt = new ArrayList<>();

            while ((line = reader.readLine()) != null) {
                tempArr = line.split(";");

                for (int i = 0; i < tempArr.length; i++) {
                    if (i == 0) {

                    } else {
                        java.lang.String temp = tempArr[i];
                        int intTemp = Integer.parseInt(temp);
                        listInt.add(intTemp);
                    }

                }
            }
            reader.close();
            return listInt;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void write(String savePlace) {

        Menu menu = new Menu();

        try {
            File file = new File(savePlace);
            if (file.createNewFile()) {
                System.out.println("File saved disini -> " + savePlace);
            }
            FileWriter writer = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(writer);

            bwr.write("Berikut Hasil Pengolahan Nilai:\n");
            bwr.write(" \n");
            bwr.write("Berikut hasil sebaran data nilai\n");

            // MEAN
            bwr.write("Mean   : " + String.format("%.2f", mean(read(menu.csvPath))));
            bwr.write("\n");

            // MEDIAN
            bwr.write("Median : " + median(read(menu.csvPath)) + "\n");

            // MODUS
            bwr.write("Modus  : " + mode(read(menu.csvPath)) + "\n");

            bwr.flush();
            bwr.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void writeMod(String saveMod) {

        Menu menu = new Menu();

        try {
            File file = new File(saveMod);
            if (file.createNewFile()) {
                System.out.println("File saved disini ->: " + saveMod);
            }
            FileWriter writer = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(writer);
            Map<Integer, Integer> hMap = freq(read(menu.csvPath));
            Set<Integer> key = hMap.keySet();
            bwr.write("Berikut Hasil Pengolahan Nilai:\n");
            bwr.write(" \n");
            bwr.write("Nilai" + "\t"   + "|\t" + "Frekuensi"  + "\n");
            for (Integer nilai : key){
                bwr.write(nilai + "\t\t" + "|\t\t" + hMap.get(nilai) + "\n");
            }

            bwr.flush();
            bwr.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // Mencari Mean
    private double mean(List<Integer> list) {
        return list.stream()
                .mapToDouble(d -> d)
                .average()
                .orElse(0.0);
    }

    // Mencari Median
    private double median(List<Integer> numArray) {
        Arrays.sort(new List[]{numArray});
        double median;
        if (numArray.size() % 2 == 0)
            median = ((double) numArray.get(numArray.size() / 2) + (double) numArray.get(numArray.size() / 2 - 1)) / 2;
        else
            median = (double) numArray.get(numArray.size() / 2);
        return median;
    }

    // Mencari Modus/Mode
    private int mode(List<Integer> array) {
        HashMap<Integer, Integer> hm = new HashMap<>();
        int max = 1;
        int temp = 0;

        for (Integer integer : array) {

            if (hm.get(integer) != null) {

                int count = hm.get(integer);
                count++;
                hm.put(integer, count);

                if (count > max) {
                    max = count;
                    temp = integer;
                }
            } else
                hm.put(integer, 1);
        }
        return temp;
    }

    // Mapping Modus
    public Map<Integer, Integer> freq(List<Integer> array) {
        Set<Integer> distinct = new HashSet<>(array);
        Map<Integer, Integer> mMap = new HashMap<>();

        for (Integer s : distinct) {
            mMap.put(s, Collections.frequency(array, s));
        }
        return mMap;
    }
}
