package com.binar.service;

import java.util.Scanner;

public class Menu implements IMainMenu {
    Scanner scan = new Scanner(System.in);

    // Pendeklarasian tempat file disimpan
    String csvPath = "src/file/data_sekolah.csv";
    String saveMeanMedianModusPath = "src/file/Mean_Median_Modus_DataSekolah.txt";
    String saveFrekuensiNilaiPath = "src/file/Frekuensi_nilai.txt";

    // Proses pada Menu Utama
    @Override
    public void switchMenu() {
        Proses hitung = new Proses();
        switch (mainMenu()) {
            case 1:
                hitung.write(saveMeanMedianModusPath);
                System.out.println("Data telah digenerate di: "+saveMeanMedianModusPath);
                System.out.println("\n\n");
                subMenu();
            case 2:
                hitung.writeMod(saveFrekuensiNilaiPath);
                System.out.println("Data telah digenerate di: "+saveFrekuensiNilaiPath);
                System.out.println("\n\n");
                subMenu();
            case 3:
                hitung.write(saveMeanMedianModusPath);
                hitung.writeMod(saveFrekuensiNilaiPath);
                System.out.println("\n\n");
                subMenu();
            case 0:
                System.out.println("Anda telah keluar dari aplikasi...");
                System.exit(0);
            default:
                System.out.println("PILIHAN TIDAK TERSEDIA! SILAHKAN PILIH LAGI");
                System.out.println("\n\n");
                switchMenu();
        }
    }

    // Proses pada Sub Menu
    @Override
    public void subMenu(){
        switch (kembali()){
            case 1:
                System.out.println("Kembali ke menu utama ...");
                System.out.println("\n\n");
                switchMenu();
            case 0:
                System.out.println("Program telah berakhir...");
                System.out.println("\n\n");
                System.exit(0);
            default:
                System.out.println("PILIHAN TIDAK TERSEDIA! SILAHKAN PILIH LAGI");
                System.out.println("\n\n");
                switchMenu();
        }
    }

    // Tampilan Menu Utama
    @Override
    public int mainMenu() {
        System.out.println("==================================================");
        System.out.println("======= Aplikasi Pengolah Data Nilai Siswa =======");
        System.out.println("--------------------------------------------------");
        System.out.println("= 1. Generate File txt Mean-Median-Modus         =");
        System.out.println("= 2. Generate File txt Modus Sekolah             =");
        System.out.println("= 3. Generate File txt Kedua File                =");
        System.out.println("= 0. Exit                                        =");
        System.out.println("==================================================");
        System.out.print  ("= Masukkan Pilihan: ");
        return scan.nextInt();
    }

    //Tampilan Sub Menu
    @Override
    public int kembali(){
        System.out.println("==================================================");
        System.out.println("======= Aplikasi Pengolah Data Nilai Siswa =======");
        System.out.println("--------------------------------------------------");
        System.out.println("= 1. Kembali ke menu utama                       =");
        System.out.println("= 0. Exit                                        =");
        System.out.println("==================================================");
        System.out.print  ("= Masukkan Pilihan: ");
        return scan.nextInt();
    }
}
