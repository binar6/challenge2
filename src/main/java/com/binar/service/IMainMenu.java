package com.binar.service;

public interface IMainMenu {
    // Deklarasikan nama kelas
    int mainMenu();
    void switchMenu();
    int kembali();
    void subMenu();
}
